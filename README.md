# source-engine
Welcome to source engine:)

Discord: https://discord.gg/hZRB7WMgGw

# source-engine-dedicated

It has been forked special for dedicated server builds even if source repository will be private for some reason.

You can download Linux dedicated release autobuilds from gitlab pipelines (ssdk-2013 or master branch) https://gitlab.com/AruMoon/source-engine-dedicated/-/pipelines

And Windows dedicated debug autobuilds only from master branch https://github.com/AruMoon/source-engine-dedicated/actions

# Current tasks
- [x] Windows build support for waf
- [x] NEON support
- [x] remove unnecessary dependencies
- [x] Arm(android) port
- [x] replace current buildsystem with waf
- [x] rewrite achivement system( to work without steam )
- [x] 64-bit support
- [x] VTF 7.5 support
- [x] PBR support
- [ ] improve performance
- [ ] fixing bugs
- [ ] dxvk-native support
- [ ] Elbrus port
- [ ] rewrite serverbrowser to work without steam


# [How to Build?](https://github.com/nillerusr/source-engine/wiki/How-to-build)
